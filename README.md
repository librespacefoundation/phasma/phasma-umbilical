# PHASMA Umbilical

PHASMA Umbilical is a CubeSat subsystem that exposes JTAG interfaces, serial ports, and PC104 pins of a satellite. This subsystem is essential for the integration and testing phases of CubeSat development, allowing for streamlined debugging and communication.

![Board Top](board-top.png)

## Introduction
PHASMA Umbilical is designed to facilitate the development and testing of CubeSat subsystems by providing easy access to critical interfaces. It is an essential tool for satellite developers to ensure their systems are functioning correctly before deployment.

## Features
- Access to JTAG interfaces
- Serial port connectivity via USB interface
- PC104 pin exposure
- Simplifies debugging and integration processes

## Documentation

[Connector pinouts](UMBILICAL_PINOUTS.md)  
[Testing procedures](TESTING.md)

## License
This project is licensed under the CERN Open Hardware License (CERN OHL). See the [LICENSE](LICENSE) file for details.

