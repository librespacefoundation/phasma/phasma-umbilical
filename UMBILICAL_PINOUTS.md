## Umbilical pinouts

### Pinout for J12 (T-Sensors):

| Pin number | Pin name | Pin net          |
|------------|----------|------------------|
| 1          | Pin\_1   | GND              |
| 2          | Pin\_2   | 1W-Bus           |
| 3          | Pin\_3   | Net-(J12-Pin\_3) |
| 4          |          |                  |
| MP         |          |                  |

### Pinout for J11 (Antennas Deploy):

| Pin number | Pin name | Pin net |
|------------|----------|---------|
| 1          | Pin\_1   | 5V      |
| 2          | Pin\_2   | SW5\_5V |
| 3          | Pin\_3   | GND     |
| 4          | Pin\_4   | 5V      |
| 5          | Pin\_5   | SW6\_5V |
| 6          | Pin\_6   | GND     |

### Pinout for J13 (COMMS FPGA DEBUG):

| Pin number | Pin name | Pin net          |
|------------|----------|------------------|
| 1          | Pin\_1   | GND              |
| 2          | Pin\_2   | COMMS\_EMMC\_CLK |
| 3          | Pin\_3   | COMMS\_EMMC\_CMD |
| 4          | Pin\_4   | COMMS\_EMMC\_D0  |
| 5          | Pin\_5   | GND              |
| 6          | Pin\_6   | COMMS\_FPGA\_TX  |
| 7          | Pin\_7   | COMMS\_FPGA\_RX  |

### Pinout for J1 (G125-MH15005L5P):

| Pin number | Pin name | Pin net             |
|------------|----------|---------------------|
| 1          | Pin\_1   | COMMS\_NJRST        |
| 2          | Pin\_2   | COMMS\_JTDI         |
| 3          | Pin\_3   | COMMS\_JTDO-SWO     |
| 4          | Pin\_4   | COMMS\_JTMS-SWDIO   |
| 5          | Pin\_5   | COMMS\_JTCK-SWCLK   |
| 6          | Pin\_6   | COMMS\_FPGA\_TDI    |
| 7          | Pin\_7   | COMMS\_FPGA\_TDO    |
| 8          | Pin\_8   | COMMS\_FPGA\_TMS    |
| 9          | Pin\_9   | COMMS\_FPGA\_TCK    |
| 10         | Pin\_10  | PAYLOAD\_NJRST      |
| 11         | Pin\_11  | PAYLOAD\_JTDI       |
| 12         | Pin\_12  | PAYLOAD\_JTDO-SWO   |
| 13         | Pin\_13  | PAYLOAD\_JTMS-SWDIO |
| 14         | Pin\_14  | PAYLOAD\_JTCK-SWCLK |
| 15         | Pin\_15  | PAYLOAD\_FPGA\_TDI  |
| 16         | Pin\_16  | PAYLOAD\_FPGA\_TDO  |
| 17         | Pin\_17  | PAYLOAD\_FPGA\_TMS  |
| 18         | Pin\_18  | PAYLOAD\_FPGA\_TCK  |
| 19         | Pin\_19  | COMMS\_EMMC\_D0     |
| 20         | Pin\_20  | COMMS\_EMMC\_CMD    |
| 21         | Pin\_21  | COMMS\_EMMC\_CLK    |
| 22         | Pin\_22  | GND                 |
| 23         | Pin\_23  | PAYLOAD\_EMMC\_D0   |
| 24         | Pin\_24  | PAYLOAD\_EMMC\_CMD  |
| 25         | Pin\_25  | PAYLOAD\_EMMC\_CLK  |
| 26         | Pin\_26  | COMMS\_MCU\_3V3     |
| 27         | Pin\_27  | COMMS\_NRST         |
| 28         | Pin\_28  | GND                 |
| 29         | Pin\_29  | GND                 |
| 30         | Pin\_30  | GND                 |
| 31         | Pin\_31  | COMMS\_FPGA\_3V3    |
| 32         | Pin\_32  | GND                 |
| 33         | Pin\_33  | GND                 |
| 34         | Pin\_34  | GND                 |
| 35         | Pin\_35  | PAYLOAD\_MCU\_3V3   |
| 36         | Pin\_36  | PAYLOAD\_NRST       |
| 37         | Pin\_37  | GND                 |
| 38         | Pin\_38  | GND                 |
| 39         | Pin\_39  | GND                 |
| 40         | Pin\_40  | PAYLOAD\_FPGA\_3V3  |
| 41         | Pin\_41  | GND                 |
| 42         | Pin\_42  | GND                 |
| 43         | Pin\_43  | GND                 |
| 44         | Pin\_44  | GND                 |
| 45         | Pin\_45  | 1W-Bus              |
| 46         | Pin\_46  | ADCS\_PPS           |
| 47         | Pin\_47  | ADCS\_BOOT          |
| 48         | Pin\_48  | ADCS\_GNSS\_RX      |
| 49         | Pin\_49  | ADCS\_GNSS\_TX      |
| 50         | Pin\_50  | GND                 |

### Pinout for J2 (G125-MH11605L7P):

| Pin number | Pin name | Pin net      |
|------------|----------|--------------|
| 1          | Pin\_1   | SW1\_12V\_P  |
| 2          | Pin\_2   | 12V\_BUS\_P  |
| 3          | Pin\_3   | VBAT\_P      |
| 4          | Pin\_4   | RBF          |
| 5          | Pin\_5   | SW5\_5V\_P   |
| 6          | Pin\_6   | RBF\_RET     |
| 7          | Pin\_7   | SW6\_5V\_P   |
| 8          | Pin\_8   | 5V\_P        |
| 9          | Pin\_9   | SW8\_3V3\_P  |
| 10         | Pin\_10  | CHARGE       |
| 11         | Pin\_11  | SW9\_3V3\_P  |
| 12         | Pin\_12  | 3V3\_BUS\_P  |
| 13         | Pin\_13  | SW10\_3V3\_P |
| 14         | Pin\_14  | GND          |
| 15         | Pin\_15  | CAN\_A\_L    |
| 16         | Pin\_16  | CAN\_A\_H    |
| 17         |          |              |
| 18         |          |              |

### Pinout for J9 (PV Deploy):

| Pin number | Pin name | Pin net                    |
|------------|----------|----------------------------|
| 1          | Pin\_1   | Deployables/SA1\_DEP1\_RTN |
| 2          | Pin\_2   | Deployables/SA2\_DEP1\_RTN |
| 3          | Pin\_3   | VBAT                       |
| 4          | Pin\_4   | VBAT                       |
| 5          | Pin\_5   | VBAT                       |
| 6          | Pin\_6   | VBAT                       |
| 7          | Pin\_7   | Deployables/SA1\_DEP2\_RTN |
| 8          | Pin\_8   | Deployables/SA2\_DEP2\_RTN |

### Pinout for J7 (RBF):

| Pin number | Pin name | Pin net  |
|------------|----------|----------|
| 1          | Pin\_1   | RBF      |
| 2          | Pin\_2   | RBF\_RET |
| 3          |          |          |

### Pinout for J6 (PAYLOAD FPGA JTAG):

| Pin number | Pin name | Pin net            |
|------------|----------|--------------------|
| 1          | Pin\_1   | PAYLOAD\_FPGA\_3V3 |
| 2          | Pin\_2   | NC                 |
| 3          | Pin\_3   | GND                |
| 4          | Pin\_4   | PAYLOAD\_FPGA\_TDI |
| 5          | Pin\_5   | GND                |
| 6          | Pin\_6   | PAYLOAD\_FPGA\_TDO |
| 7          | Pin\_7   | GND                |
| 8          | Pin\_8   | PAYLOAD\_FPGA\_TMS |
| 9          | Pin\_9   | GND                |
| 10         | Pin\_10  | PAYLOAD\_FPGA\_TCK |
| MP         |          |                    |

### Pinout for J10 (PV Deploy BK):

| Pin number | Pin name | Pin net          |
|------------|----------|------------------|
| 1          | Pin\_1   | Net-(J10-Pin\_1) |
| 2          | Pin\_2   | GND              |
| 3          | Pin\_3   | Net-(J10-Pin\_3) |
| 4          | Pin\_4   | GND              |

### Pinout for J5 (PAYLOAD MCU JTAG):

| Pin number | Pin name | Pin net             |
|------------|----------|---------------------|
| 1          | Pin\_1   | PAYLOAD\_MCU\_3V3   |
| 2          | Pin\_2   | PAYLOAD\_NJRST      |
| 3          | Pin\_3   | PAYLOAD\_NRST       |
| 4          | Pin\_4   | PAYLOAD\_JTDI       |
| 5          | Pin\_5   | GND                 |
| 6          | Pin\_6   | PAYLOAD\_JTDO-SWO   |
| 7          | Pin\_7   | GND                 |
| 8          | Pin\_8   | PAYLOAD\_JTMS-SWDIO |
| 9          | Pin\_9   | GND                 |
| 10         | Pin\_10  | PAYLOAD\_JTCK-SWCLK |
| MP         |          |                     |

### Pinout for J4 (COMMS FPGA JTAG):

| Pin number | Pin name | Pin net          |
|------------|----------|------------------|
| 1          | Pin\_1   | COMMS\_FPGA\_3V3 |
| 2          | Pin\_2   | NC               |
| 3          | Pin\_3   | GND              |
| 4          | Pin\_4   | COMMS\_FPGA\_TDI |
| 5          | Pin\_5   | GND              |
| 6          | Pin\_6   | COMMS\_FPGA\_TDO |
| 7          | Pin\_7   | GND              |
| 8          | Pin\_8   | COMMS\_FPGA\_TMS |
| 9          | Pin\_9   | GND              |
| 10         | Pin\_10  | COMMS\_FPGA\_TCK |
| MP         |          |                  |

### Pinout for J3 (COMMS MCU JTAG):

| Pin number | Pin name | Pin net           |
|------------|----------|-------------------|
| 1          | Pin\_1   | COMMS\_MCU\_3V3   |
| 2          | Pin\_2   | COMMS\_NJRST      |
| 3          | Pin\_3   | COMMS\_NRST       |
| 4          | Pin\_4   | COMMS\_JTDI       |
| 5          | Pin\_5   | GND               |
| 6          | Pin\_6   | COMMS\_JTDO-SWO   |
| 7          | Pin\_7   | GND               |
| 8          | Pin\_8   | COMMS\_JTMS-SWDIO |
| 9          | Pin\_9   | GND               |
| 10         | Pin\_10  | COMMS\_JTCK-SWCLK |
| MP         |          |                   |

### Pinout for J14 (PAYLOAD FPGA DEBUG):

| Pin number | Pin name | Pin net            |
|------------|----------|--------------------|
| 1          | Pin\_1   | GND                |
| 2          | Pin\_2   | PAYLOAD\_EMMC\_CLK |
| 3          | Pin\_3   | PAYLOAD\_EMMC\_CMD |
| 4          | Pin\_4   | PAYLOAD\_EMMC\_D0  |
| 5          | Pin\_5   | GND                |
| 6          | Pin\_6   | PAYLOAD\_FPGA\_TX  |
| 7          | Pin\_7   | PAYLOAD\_FPGA\_RX  |
