import argparse
import json
from time import sleep
import serial
import serial.tools.list_ports
import tqdm


def check_echo(port):
    """Checks if characters are echoed on a serial port."""
    port.write(b'A')
    response = port.read(1)
    return response == b'A'


def toggle_dtr_dsr(port):
    """Toggles DTR and checks if DSR follows the state."""
    port.setDTR(True)
    sleep(0.1)
    dtr_high_status = True if port.getDSR() else False
    port.setDTR(False)
    sleep(0.1)
    dtr_low_status = True if not port.getDSR() else False
    return True if dtr_high_status and dtr_low_status else False


def print_table(ports, markdown=False):
    if markdown:
        table_header = '| Port | RxTx_loop | DxR_loop |\n|---|---|---|'
        table_row = '| {} | {} | {} |'
    else:
        table_header = 'Port\t\tRxTx_loop\tDxR_loop'
        table_row = '{:<10}\t{:<10}\t\t{:<10}'

    print(table_header)
    for port, echo_status, dtr_loop_status in ports:
        echo_color = '\033[32m' if echo_status else '\033[31m'
        dtr_loop_color = '\033[32m' if dtr_loop_status else '\033[31m'
        reset_color = '\033[0m'
        print(table_row.format(port, f"{echo_color}{
            'Pass' if echo_status else 'Fail'}{reset_color}", f"{
                dtr_loop_color}{'Closed' if dtr_loop_status else 'Open'}{
            reset_color}"))


def print_json(ports):
    json_data = {port: {'rx_tx_loop': echo_status, 'dtr_loop': dtr_loop_status}
                 for port, echo_status, dtr_loop_status in ports}
    print(json.dumps(json_data, indent=4))


def main():
    parser = argparse.ArgumentParser(
        description='Check serial port echo and DTR/DSR behavior.')
    parser.add_argument('--ports', type=str, default='0-3',
                        help='Range of serial ports to check (e.g., 0-3)')
    parser.add_argument('--device', type=str, default='/dev/ttyUSB',
                        help='Base device path for serial ports')
    parser.add_argument('--detect', type=str,
                        help='USB device name to search for')
    parser.add_argument('--markdown', action='store_true',
                        help='Output results in Markdown format')
    parser.add_argument('--json', action='store_true',
                        help='Output results in JSON format')
    args = parser.parse_args()

    if args.detect:
        ports = {p.device: p.product for p in serial.tools.list_ports.comports(
        ) if args.detect.lower() in str(p.product).lower()}
        print('Detected ports:')
        for device, product in ports.items():
            print(f'{product}: {device}')
    else:
        start, end = map(int, args.ports.split('-'))
        ports = [f'{args.device}{port_num}' for port_num in range(start, end + 1)]

    failed_ports = []
    results = []
    for port_path in tqdm.tqdm(ports, desc='Checking ports', unit='port'):
        try:
            with serial.Serial(port_path, timeout=0.5) as port:
                echo_status = True if check_echo(port) else False
                dtr_loop_status = toggle_dtr_dsr(port)
                results.append((port.name, echo_status, dtr_loop_status))
        except serial.SerialException:
            failed_ports.append(port_path)

    if args.json:
        print_json(results)
    else:
        print_table(results, args.markdown)
        if failed_ports:
            print('The following ports faild to open:', failed_ports)


if __name__ == '__main__':
    main()
