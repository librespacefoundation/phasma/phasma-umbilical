#!/bin/bash

if ! command -v ftdi_eeprom &> /dev/null; then
    echo "ftdi_eeprom is not installed. Please install it first."
    exit 1
fi

# Parse command-line arguments
while [[ $# -gt 0 ]]; do
    case "$1" in
        --program|-p)
            program_mode=true
            shift 1
            ;;
        *)
            serial_number="$1"
            shift 1
            ;;
    esac
done

if [[ -z "$serial_number" ]]; then
    echo "Usage: $0 <serial_number> [--program|-p]"
    exit 1
fi

conf_filename="FTDI_$serial_number.conf"
bin_filename="FTDI_$serial_number.bin"
echo 'vendor_id=0x0403' > $conf_filename
echo 'product_id=0x6011' >> $conf_filename
echo 'manufacturer=LSF' >> $conf_filename
echo 'product="CubeSat Umbilical Rev-1.0"' >> $conf_filename
echo "serial=\"$serial_number\"" >> $conf_filename
echo "use_serial=true" >> $conf_filename
echo "filename=$bin_filename" >> $conf_filename

ftdi_eeprom --build-eeprom $conf_filename --device i:0x0403:0x6011

if [[ "$program_mode" == true ]]; then
    ftdi_eeprom --flash-eeprom $conf_filename --device i:0x0403:0x6011
fi