# Testing utilities

This is a set of testing and setup utilities for CubeSat Umbilical

- [Testing utilities](#testing-utilities)
- [Serial Port Echo and DTR/DSR Tester](#serial-port-echo-and-dtrdsr-tester)
  - [Features](#features)
  - [Usage](#usage)
  - [Installation](#installation)
- [EEPROM programmer](#eeprom-programmer)
- [License](#license)

# Serial Port Echo and DTR/DSR Tester

This Python script tests the echo functionality and DTR/DSR loopback behavior of serial ports. It also filters by device name information for USB-connected serial devices.

## Features

- Tests echo loopback on serial ports.
- Tests DTR/DSR loopback for functionality.
- Supports filtering ports based on a regular expression for device name.
- Provides output in Markdown or JSON format.

## Usage

```
python uart_test.py [--ports PORTS] [--device DEVICE] [--detect device_name] [--markdown] [--json]
```

**Arguments:**

- `--ports`: (Optional) Range of serial ports to check (e.g., 0-3).
- `--device`: (Optional) Base device path for serial ports (e.g. /dev/ttyUSB).
- `--detect`: (Optional) Regular expression to filter ports by USB device name.
- `--markdown`: (Optional) Output results in Markdown format (default: plain text).
- `--json`: (Optional) Output results in JSON format.

**Requirements:**

- `pyserial`
- `usb.core` (part of `pyusb`)
- `tqdm` (optional, for progress bar)

**Example:**

```
python uart_test.py --device-name "FTDI" --markdown
```

This command checks serial ports and displays the results in a Markdown table, filtering ports whose names match the regular expression "FTDI" (case-insensitive).

## Installation

1. Install the required libraries:

   ```bash
   pip install -r requirements.txt
   ```

2. Run the script:

   ```bash
   python uart_test.py
   ```
# EEPROM programmer

**Description:**

This Bash script provides a simple interface for interacting with FTDI devices using the `ftdi_eeprom` tool. It allows you to program the EEPROM of Umbilical  FTDI device.

**Usage:**

```bash
ftdi_eeprom_script <serial_number> [--program|-p]
```

**Arguments:**

- **serial_number:** The serial number of the FTDI device.
- **--program|-p:** Optional flag to indicate that the device should be programmed instead of having its information read.

**Functionality:**

- Generates `ftdi_eeprom` configuration and binary file based on the provided serial number.
- Programs Umbilical EEPROM

**Example:**

To generate the configuration and binary file of an Umbilical device with serial number `UMB-123456`, you would run:

```bash
./eeprom_gen.sh UMB-123456
```

To program the same device, you would run:

```bash
./eeprom_gen.sh UMB-123456 --program
```

**Note:**

- Ensure that `ftdi_eeprom` is installed on your system before running this script.


# License
This project is licensed under the [GNU General Public License v3 (GPL-3.0)](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE) file for more details.
