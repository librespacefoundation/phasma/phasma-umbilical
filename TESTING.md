# TESTING

- [TESTING](#testing)
  - [USB Connection](#usb-connection)
  - [USB Serial ports (Automated)](#usb-serial-ports-automated)
  - [USB Serial ports (Manual)](#usb-serial-ports-manual)
    - [Serial port 1 and 2](#serial-port-1-and-2)
    - [Serial port 3](#serial-port-3)
    - [Serial port 4](#serial-port-4)
    - [Test USB Serial port to 1-Wire connection](#test-usb-serial-port-to-1-wire-connection)
  - [Thermal knife controllers](#thermal-knife-controllers)
    - [SA1 DEP1](#sa1-dep1)
    - [SA2 DEP1](#sa2-dep1)
    - [SA1 DEP2](#sa1-dep2)
    - [SA2 DEP2](#sa2-dep2)
  - [Onboard temperature sensor](#onboard-temperature-sensor)
  - [External temperature sensor(s)](#external-temperature-sensors)

## USB Connection
Umbilical uses FT4232 exposing 4 serial ports via USB.  
Verify device detection by monitoring the system logs  
```bash
sudo dmesg -w
```
Connect Umbilical via USB
```
[nnnnnn.264340] usb 1-2: new high-speed USB device number 71 using xhci_hcd
[nnnnnn.395052] usb 1-2: New USB device found, idVendor=0403, idProduct=6011, bcdDevice= 8.00
[nnnnnn.395126] usb 1-2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[nnnnnn.395136] usb 1-2: Product: CubeSat Umbilical Rev-1.0
[nnnnnn.395143] usb 1-2: Manufacturer: LSF
[nnnnnn.395148] usb 1-2: SerialNumber: 00001
[nnnnnn.402126] ftdi_sio 1-2:1.0: FTDI USB Serial Device converter detected
[nnnnnn.402265] usb 1-2: Detected FT4232H
[nnnnnn.402634] usb 1-2: FTDI USB Serial Device converter now attached to ttyUSB0
[nnnnnn.405894] ftdi_sio 1-2:1.1: FTDI USB Serial Device converter detected
[nnnnnn.406021] usb 1-2: Detected FT4232H
[nnnnnn.406630] usb 1-2: FTDI USB Serial Device converter now attached to ttyUSB1
[nnnnnn.409679] ftdi_sio 1-2:1.2: FTDI USB Serial Device converter detected
[nnnnnn.409781] usb 1-2: Detected FT4232H
[nnnnnn.410026] usb 1-2: FTDI USB Serial Device converter now attached to ttyUSB2
[nnnnnn.413329] ftdi_sio 1-2:1.3: FTDI USB Serial Device converter detected
[nnnnnn.413434] usb 1-2: Detected FT4232H
[nnnnnn.413714] usb 1-2: FTDI USB Serial Device converter now attached to ttyUSB3
```
If Manufacturer or Product are not as above or serial number is not populated use the eeprom programming utility to populate these fields.
For details and usage see [README](testing/README.md)

## USB Serial ports (Automated)

Serial ports can be tested using `uart_test.py` utility. See [README](testing/README.md) for usage

**Steps**
1. Connect picoblade loop cables to J13 and J14
2. Bridge H1 pins 17 & 18
3. H1 pins 19 & 20
4. Run test utility
   
```bash
python3 uart_test.py --detect Umbilical
```
5. DTR loop should be present in port 1 (1-Wire connection)

## USB Serial ports (Manual)

### Serial port 1 and 2

Perform loopback testing for each serial port using a loopback cable. A simple loopback cable can be built using a picoblade connector having pin 6 connected to pin 7.  
Serial port 1 is exposed on J13 pins 6 & 7.  
Serial port 2 is exposed on J14 pins 6 & 7.  
**Steps**
1. Open a terminal to serial port
2. Configure terminal to not echo typed characters[1]
3. Type some words. No output should be visible
4. Connect loopback cable to J13
5. Type some words. Output should be visible

Repeat for 2nd serial port

### Serial port 3
Serial port 3 is exposed on H1 pins 19 & 20

**Steps**
1. Open a terminal to serial port
2. Configure terminal to not echo typed characters[1]
3. Type some words. No output should be visible
4. Bridge H1 pin 19 to H1 pin 20 using a jumper cable
5. Type some words. Output should be visible

### Serial port 4
Serial port 4 is exposed on H1 pins 17 & 18

**Steps**
1. Open a terminal to serial port
2. Configure terminal to not echo typed characters[1]
3. Type some words. No output should be visible
4. Bridge H1 pin 17 to H1 pin 18 using a jumper cable
5. Type some words. Output should be visible

### Test USB Serial port to 1-Wire connection
1-Wire bus is also connected to serial port 1 DTR/DSR pins  

Using a serial port application that can toggle DTR check whether DSR follows DTR's state

## Thermal knife controllers
Prepare temperature measurement equipment for measuring temperature of Q1 and Q2.  
Use a DC load in CR mode. Set resistance to **3.25Ω**  
Connect DC Load + to one of the VBat pins of **J9**  
Connect DC Load - to **J9-1**

Pinout for J9 (PV Deploy):
| Pin number | Pin name | 
|------------|----------|
| 1          | SA1\_DEP1\_RTN |
| 2          | SA2\_DEP1\_RTN |
| 3-6        | VBAT           |
| 7          | SA1\_DEP2\_RTN |
| 8          | SA2_DEP2\_RTN |

Connect a PSU set to **8.365V** to H2-45/46 and GND  
Verify that DC Load - is not connected indirectly to ground in any way  

### SA1 DEP1
Apply **12V** to **H2-8** for at least 26s  
Measure current  
Measure top Temperature

### SA2 DEP1
Connect DC Load - to **J9-2**  
Apply **12V** to **H2-10** for at least 26s  
Measure current  
Measure top Temperature

### SA1 DEP2
Connect DC Load - to **J9-7**  
Apply **5V** to **J10-1** for at least 26s  
Measure current  
Measure top Temperature

### SA2 DEP2
Connect DC Load - to **J9-8**  
Apply **5V** to **J10-3** for at least 26s  
Measure current  
Measure top Temperature

## Onboard temperature sensor
Temperature sensors are used only during testing. They are powered via the USB Bus and operate at 3.3V.  
1-Wire bus is available on J1-45.  
See [Datasheet](https://www.ti.com/lit/ds/symlink/tmp1826.pdf) for communication protocol

## External temperature sensor(s)
Connect external sensors to J12.  
Follow [Onboard Temperature Sensor](#onboard-temperature-sensor) testing setup.  
Read all sensor IDs and Temperatures on the bus  